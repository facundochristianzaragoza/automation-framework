package automation.pages;

import automation.driver.DriverSingleton;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import automation.utils.Constants;

public abstract class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    private final Actions actions;
    private final JavascriptExecutor JS_EXECUTOR;

    protected BasePage() {
        this.driver = DriverSingleton.getDriver();
        this.wait = new WebDriverWait(driver, Constants.TIME_OUT_15_SECONDS);
        this.JS_EXECUTOR = (JavascriptExecutor)driver;
        this.actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    protected void write(WebElement webElement, String text) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        webElement.sendKeys(text);
    }

    protected void waitAndClick(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    protected void waitElementSelectionStateToBeFalseAndClick(WebElement element) {
        wait.until(ExpectedConditions.elementSelectionStateToBe(element, false));
        element.click();
    }

    protected void waitAndClickJavascriptExecutor(WebElement element) {
        wait.until(ExpectedConditions.invisibilityOf(element));
        JS_EXECUTOR.executeScript("arguments[0].click();", element);
    }

    protected String waitAndGetElementText(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.getText();
    }

    protected void hoverToElement(WebElement element) {
        actions.moveToElement(element).build().perform();
    }
}
