package automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {

    private static final String ADD_TO_CART_BUTTON_BY = "a[title='Add to cart']";

    @FindBy(css = "ul#homefeatured > li")
    private List<WebElement> products;

    @FindBy(css = "a[title='View my shopping cart']")
    private WebElement cartButton;

    @FindBy(css = "span[title='Continue shopping']")
    private WebElement continueShoppingButton;

    @FindBy(css = "a[title='Proceed to checkout']")
    private WebElement proceedToCheckOutButton;

    @FindBy(css = "a.login")
    private WebElement signInButton;

    @FindBy(id = "search_query_top")
    private WebElement searchBox;

    @FindBy(css = "button[name='submit_search']")
    private WebElement searchButton;

    @FindBy(css = "ul * div.product-image-container")
    private WebElement searchResult;

    public void addProductToCart(int productOrder) {
        WebElement productCard = products.get(productOrder);
        WebElement addToCartButton = productCard.findElement(By.cssSelector(ADD_TO_CART_BUTTON_BY));
        hoverToElement(productCard);
        waitAndClick(addToCartButton);

    }

    public String getProductsQuantity() {
        return waitAndGetElementText(cartButton);
    }

    public void clickContinueShopping() {
        waitAndClick(continueShoppingButton);
    }

    public CheckoutPage clickProceedToCheckOut() {
        waitAndClick(proceedToCheckOutButton);
        return new CheckoutPage();
    }

    public SignInPage clickSignInButton() {
        waitAndClick(signInButton);
        return new SignInPage();
    }

    public boolean searchElement(String searchStr) {
        write(searchBox, searchStr);
        waitAndClick(searchButton);

        try {
            if (searchResult.isEnabled()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }

        return false;
    }

}
