package automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import automation.utils.Utils;

public class SignInPage extends BasePage {

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    private WebElement signInButton;

    @FindBy(id = "email_create")
    private WebElement emailSignUpField;

    @FindBy(id = "SubmitCreate")
    private WebElement signUpButton;

    @FindBy(css = "a.account > span")
    private WebElement username;

    public CheckoutPage login(String username, String password) {
        System.out.println("Filling the box with user: " + username + " and password (not decoded): " + password);
        write(emailField, username);
        write(passwordField, Utils.decode64(password));
        waitAndClick(signInButton);
        return new CheckoutPage();
    }

    public void signUp(String email) {
        write(emailSignUpField, email);
        waitAndClick(signUpButton);
    }

    public String getUsername() {
        return waitAndGetElementText(username);
    }
}
