package automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import automation.utils.Constants;

public class CheckoutPage extends BasePage{

    @FindBy(css = "p > a[title='Proceed to checkout']")
    private WebElement checkoutButtonSummary;

    @FindBy(css = "button[name='processAddress']")
    private WebElement checkoutButtonConfirmAddress;

    @FindBy(css = "button[name='processCarrier']")
    private WebElement checkoutButtonConfirmShipping;

    @FindBy(css = "head > title")
    private WebElement pageTitle;

    @FindBy(id = "cgv")
    private WebElement confirmShippingCheckbox;

    @FindBy(css = "a[title='Pay by bank wire']")
    private WebElement payByBankWireOption;

    @FindBy(css = "p#cart_navigation > button[type='submit']")
    private WebElement confirmOrderButton;

    @FindBy(css = "p.cheque-indent > strong.dark")
    private WebElement orderConfirmationMessage;

    @FindBy(id = "summary_products_quantity")
    private WebElement productsQuantity;

    public boolean checkTitle(String title) {
        return waitAndGetElementText(pageTitle).equals(title);
    }

    public SignInPage goToCheckOut() {
        waitAndClick(checkoutButtonSummary);
        return new SignInPage();
    }

    public void confirmAddress() {
        waitAndClick(checkoutButtonConfirmAddress);
    }

    public void confirmShipping() {
        waitElementSelectionStateToBeFalseAndClick(confirmShippingCheckbox);
        waitAndClick(checkoutButtonConfirmShipping);
    }

    public void payByBankWire() {
        waitAndClick(payByBankWireOption);
    }

    public void confirmFinalOrder() {
        waitAndClick(confirmOrderButton);
    }

    public boolean checkFinalStatus() {
        return waitAndGetElementText(orderConfirmationMessage).equals(Constants.COMPLETE_ORDER_MESSAGE);
    }

    public String getProductsQuantity() {
        return waitAndGetElementText(productsQuantity);
    }
}
