package automation.driver;

import automation.driver.strategies.DriverStrategy;
import automation.driver.strategies.DriverStrategyImplementer;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class DriverSingleton {

    private static DriverSingleton instance = null;
    private static WebDriver driver;

    private DriverSingleton(String driver) {
        instantiate(driver);
    }

    private static void instantiate(String browser) {
        DriverStrategy driverStrategy = DriverStrategyImplementer.chooseStrategy(browser);
        if (driverStrategy != null) {
            driver = driverStrategy.setStrategy();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.manage().window().maximize();

        } else {
            throw new InvalidArgumentException("The strategy for the browser: " + browser + "doesn't exist");
        }
    }

    public static void getInstance(String driver) {
        if (instance == null) {
            instance = new DriverSingleton(driver);
        }
    }

    public static void closeObjectInstance() {
        instance = null;
        driver.quit();
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
