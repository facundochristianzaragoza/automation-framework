package automation.driver.strategies;

import automation.utils.Constants;

public class DriverStrategyImplementer {

    private DriverStrategyImplementer() {
    }

    public static DriverStrategy chooseStrategy(String browser) {
        switch(browser){
            case Constants.CHROME:
                return new Chrome();
            case Constants.FIREFOX:
                return new Firefox();
            case Constants.PHANTOMJS:
                return new PhantomJs();
            default:
                return null;
        }
    }
}
