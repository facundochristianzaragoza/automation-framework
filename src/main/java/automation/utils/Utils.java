package automation.utils;

import automation.driver.DriverSingleton;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Base64;

public class Utils {

    public static int testCount = 0;

    public static String decode64(String encodedString) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedString.getBytes()));
    }

    public static boolean takeScreenshot() {
        LocalDateTime localDateTime = LocalDateTime.now();
        TakesScreenshot takesScreenshot = (TakesScreenshot) DriverSingleton.getDriver();
        File file = takesScreenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileCopyUtils.copy(file, new File(String.format(Constants.SCREENSHOT_NAME_TEMPLATE,
                    localDateTime).replace(":", "_")));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Utils() {
    }


}
