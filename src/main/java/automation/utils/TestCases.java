package automation.utils;

public enum TestCases {
    T1("Testing the authentication"),
    T2("Testing the purchases of two items");

    private String testName;

    private TestCases(String testName) {
        this.testName = testName;
    }

    public String getTestName() {
        return testName;
    }
}
