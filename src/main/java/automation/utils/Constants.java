package automation.utils;

public class Constants {

    public static final String PROPERTIES_FILE_NAME = "framework.properties";
    public static final String FILE_NOT_FOUND_EXCEPTION_MESSAGE = "The Property file has not been found";
    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String PHANTOMJS = "PhantomJs";
    public static final int TIME_OUT_15_SECONDS = 15;
    public static final String COMPLETE_ORDER_MESSAGE = "Your order on My Store is complete.";
    public static final String EMAIL_KEY = "email";
    public static final String PASSWORD_KEY = "password";
    public static final String BROWSER_KEY = "browser";
    public static final String BASE_URL_KEY = "baseurl";
    public static final String USERNAME_KEY = "username";
    public static final String CART_QUANTITY_TEST_2 = "2 Products";
    public static final String SCREENSHOT_NAME_TEMPLATE = "screenshots\\Screenshot_%s.png";

    private Constants() {
    }
}
