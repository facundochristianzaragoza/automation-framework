package automation.glue;

import automation.config.AutomationFrameworkConfiguration;
import automation.driver.DriverSingleton;
import automation.pages.CheckoutPage;
import automation.pages.HomePage;
import automation.pages.SignInPage;
import automation.utils.ConfigurationProperties;
import automation.utils.Log;
import automation.utils.TestCases;
import automation.utils.Utils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.After;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = AutomationFrameworkConfiguration.class)
public class StepDefinition {

    private HomePage homePage;
    private SignInPage signInPage;
    private CheckoutPage checkoutPage;
    protected WebDriver driver;
    ExtentTest test;
    static ExtentReports report = new ExtentReports("report/TestReport.html");

    @Autowired
    protected ConfigurationProperties configurationProperties;

    @Before
    public void setUp() {
        DriverSingleton.getInstance(configurationProperties.getBrowser());
        driver = DriverSingleton.getDriver();
        homePage = new HomePage();
        signInPage = new SignInPage();
        checkoutPage = new CheckoutPage();
        TestCases[] tests = TestCases.values();
        String testName = tests[Utils.testCount].getTestName();
        test = report.startTest(testName);
        Log.getLogData(Log.class.getName());
        Log.startTest(testName);
        Utils.testCount++;
    }

    @Given("^I go to the Website")
    public void i_go_to_the_website() {
        Log.info("Navigating to " + configurationProperties.getBaseUrl());
        driver.get(configurationProperties.getBaseUrl());
        test.log(LogStatus.PASS, "Navigation to " + configurationProperties.getBaseUrl());
    }

    @When("^I click on the Sign In button")
    public void i_click_on_the_sign_in_button() {
        homePage.clickSignInButton();
        test.log(LogStatus.PASS, "Sign In button has been clicked.");
    }

    @When("^I add two elements to the cart")
    public void i_add_two_elements_to_the_cart() {
        homePage.addProductToCart(0);
        homePage.addProductToCart(6);
        test.log(LogStatus.PASS, "Two elements were added to the cart.");
    }

    @And("^I specify my credentials and click in the Login button")
    public void i_specify_my_credentials_and_click_in_the_login_button() {
        signInPage.login(configurationProperties.getEmail(), configurationProperties.getPassword());
        test.log(LogStatus.PASS, "The credentials were specified and login has been clicked.");
    }

    @And("^I proceed to checkout")
    public void i_proceed_to_checkout() {
        homePage.clickProceedToCheckOut();
        test.log(LogStatus.PASS, "We proceed to checkout.");
    }

    @And("^I confirm address, shipping, payment and final order")
    public void i_confirm_address_shipping_payment_and_final_order() {
        checkoutPage.goToCheckOut();
        signInPage.login(configurationProperties.getEmail(), configurationProperties.getPassword());
        checkoutPage.confirmAddress();
        checkoutPage.confirmShipping();
        checkoutPage.payByBankWire();
        checkoutPage.confirmFinalOrder();
        test.log(LogStatus.PASS, "We confirm the final order.");
    }

    @Then("^I can log into the Website")
    public void i_can_log_into_the_website() {
        Assert.assertEquals("The user name in the page is not equals that the username used to sign in",
                configurationProperties.getUsername(), signInPage.getUsername());
        test.log(LogStatus.PASS, "The authentication is successful.");
    }

    @Then("^The elements are bought")
    public void the_elements_are_bought() {
        Assert.assertTrue("The elements are not bought", checkoutPage.checkFinalStatus());
        test.log(LogStatus.PASS, "The two items are bought.");
    }

    @After
    public void tearDown() {
        report.endTest(test);
        report.flush();
        DriverSingleton.closeObjectInstance();
        driver.quit();
    }

}
